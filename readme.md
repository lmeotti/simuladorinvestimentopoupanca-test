# SimuladorInvestimentoPoupanca-Test

Projeto de automação Web e Api.

Foi dada prioridade aos testes de aceitação, visando automatizar o fluxo do usuário,
garantindo assim, a funcionalidade.

## Configurações Necessárias
 - Para executar o projeto é preciso possuir:
    - Java JDK 11;
    - Gradle 6.3;
    - Ambiente Selenium Configurado (com Chromedriver);
    
## Execução
 - É possivel realizar a execução abrindo-o em uma IDE de sua preferência ou,
  acessando o diretório do projeto, através do terminal do seu sistema operacional
  e rodar o comando "gradle clean test".
package br.com.sicredi.simulador.investimento.poupanca.apiTests;

import br.com.sicredi.simulador.investimento.poupanca.report.ReportConfiguration;
import br.com.sicredi.simulador.investimento.poupanca.specifications.RequestSpec;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeMethod;

public class BaseTestApi extends ReportConfiguration {

    public static RequestSpecification specification;

    @BeforeMethod
    public void sepUp(){
        specification = RequestSpec.specGetSimulacao();
    }
}

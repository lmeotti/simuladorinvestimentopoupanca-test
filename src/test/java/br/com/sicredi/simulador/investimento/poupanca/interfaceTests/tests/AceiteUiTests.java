package br.com.sicredi.simulador.investimento.poupanca.interfaceTests.tests;

import br.com.sicredi.simulador.investimento.poupanca.interfaceTests.BaseTestUi;
import br.com.sicredi.simulador.investimento.poupanca.objects.DadosSimulacaoDTO;
import org.testng.annotations.Test;

public class AceiteUiTests extends BaseTestUi {

    @Test
    public void simularPeriodoMensalParaVoceTest(){
        //Realiza a geração do objeto com os dados para simulação
        DadosSimulacaoDTO dadosParaAplicacao = DadosSimulacaoDTO.
                                                        builder().
                                                            valorAplicacaoInicial(100.00).
                                                            valorAPoupar(100.00).
                                                            meses(true).
                                                            periodo(3).
                                                        build()
        ;

        //Realiza a ação do teste
        simuladorTask.selecionarPerfil(true);
        simuladorTask.preencherFormulario(dadosParaAplicacao);
        simuladorTask.realizarSimulacao();

        //Valida o teste
        simuladorTask.validarSimulacao(dadosParaAplicacao);
    }

    @Test
    public void simularPeriodoMensalParaEmpresaTest(){
        //Realiza a geração do objeto com os dados para simulação
        DadosSimulacaoDTO dadosParaAplicacao = DadosSimulacaoDTO.
                                                        builder().
                                                            valorAplicacaoInicial(20.00).
                                                            valorAPoupar(20.00).
                                                            meses(true).
                                                            periodo(12).
                                                        build()
        ;

        //Realiza a ação do teste
        simuladorTask.selecionarPerfil(false);
        simuladorTask.preencherFormulario(dadosParaAplicacao);
        simuladorTask.realizarSimulacao();

        //Valida o teste
        simuladorTask.validarSimulacao(dadosParaAplicacao);
    }

    @Test
    public void valorMenorQueMinimoTest(){
        //Realiza a geração do objeto com os dados para simulação
        DadosSimulacaoDTO dadosParaAplicacao = DadosSimulacaoDTO.
                                                        builder().
                                                            valorAplicacaoInicial(19.99).
                                                            valorAPoupar(19.99).
                                                            meses(true).
                                                            periodo(12).
                                                        build()
        ;

        //Realiza a ação do teste
        simuladorTask.selecionarPerfil(true);
        simuladorTask.preencherFormulario(dadosParaAplicacao);
        simuladorTask.realizarSimulacao();

        //Valida o teste
        simuladorTask.validarValorMinimoFormulario();
    }

    @Test
    public void naoPreencherFormularioTest(){

        //Realiza a ação do teste
        simuladorTask.selecionarPerfil(true);
        simuladorTask.realizarSimulacao();

        //Valida o teste
        simuladorTask.validarPreenchimentoObrigatorioFormulario();
    }

    @Test
    public void refazerSimulacaoTest(){
        //Realiza a geração do objeto com os dados para simulação
        DadosSimulacaoDTO dadosParaAplicacao = DadosSimulacaoDTO.
                                                        builder().
                                                            valorAplicacaoInicial(100.00).
                                                            valorAPoupar(100.00).
                                                            meses(true).
                                                            periodo(3).
                                                        build()
        ;

        //Realiza a ação do teste
        simuladorTask.selecionarPerfil(true);
        simuladorTask.preencherFormulario(dadosParaAplicacao);
        simuladorTask.realizarSimulacao();
        simuladorTask.refazerSimulacao();

        simuladorTask.validarRefazerSimulacao();
    }
}

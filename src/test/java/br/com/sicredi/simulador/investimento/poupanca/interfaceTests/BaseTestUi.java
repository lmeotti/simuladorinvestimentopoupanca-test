package br.com.sicredi.simulador.investimento.poupanca.interfaceTests;


import br.com.sicredi.simulador.investimento.poupanca.environment.LoaderProperties;
import br.com.sicredi.simulador.investimento.poupanca.report.ReportConfiguration;
import br.com.sicredi.simulador.investimento.poupanca.tasks.SimuladorTask;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class BaseTestUi extends ReportConfiguration {

    private static WebDriver driver;
    public SimuladorTask simuladorTask;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeMethod
    public void setUp() {

        driver = new ChromeDriver();
        driver.manage().window().fullscreen();
        driver.get(LoaderProperties.getUrlSimulador());

        simuladorTask = new SimuladorTask(driver);
    }


    @AfterTest
    public void turnDown(){
        if (driver != null) {
            driver.quit();
        }
    }
}

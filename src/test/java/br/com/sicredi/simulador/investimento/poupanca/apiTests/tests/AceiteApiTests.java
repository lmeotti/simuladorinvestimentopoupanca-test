package br.com.sicredi.simulador.investimento.poupanca.apiTests.tests;

import br.com.sicredi.simulador.investimento.poupanca.apiTests.BaseTestApi;
import io.restassured.path.json.JsonPath;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class AceiteApiTests extends BaseTestApi {

    @Test
    public void realizarSimulacaoNotNullValues(){
        given()
                .spec(specification)
            .when()
                .get()
            .then()
                .statusCode(200)
                .contentType("application/json")
                .body("id", notNullValue())
                .body("meses", notNullValue())
                .body("meses", not(empty()))
                .body("valor", notNullValue())
                .body("valor", not(empty()))
        ;
    }

    @Test
    public void realizarSimulacaoMethodPost(){
        given()
                .spec(specification)
            .when()
                .post()
            .then()
                .statusCode(400)
                .body(is("\"Max number of elements reached for this resource!\""))
        ;
    }

    @Test
    public void realizarSimulacaoMethodDelete(){
        given()
                .spec(specification)
            .when()
                .delete()
            .then()
                .statusCode(400)
                .body("msg", is("Invalid request"))
        ;
    }

    @Test
    public void realizarSimulacaoMethodPatch(){
        given()
                .spec(specification)
            .when()
                .patch()
            .then()
                .statusCode(400)
                .body("msg", is("Invalid request"))
        ;
    }

    @Test
    public void realizarSimulacaoMethodPut(){
        given()
                .spec(specification)
            .when()
                .put()
            .then()
                .statusCode(400)
                .body("msg", is("Invalid request"))
        ;
    }
}

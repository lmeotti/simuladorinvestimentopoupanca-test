package br.com.sicredi.simulador.investimento.poupanca.apiTests.tests;

import br.com.sicredi.simulador.investimento.poupanca.apiTests.BaseTestApi;
import io.restassured.module.jsv.JsonSchemaValidator;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class ContratoTest extends BaseTestApi {


    @Test
    public void validarContratoApi(){
        given()
                .spec(specification)
                .when()
                .get()
                .then()
                .statusCode(200)
                .contentType("application/json")
                .body(JsonSchemaValidator.matchesJsonSchemaInClasspath("jsonSchema/SimuladorApiSchema.json"))
        .extract().jsonPath()
        ;
    }
}

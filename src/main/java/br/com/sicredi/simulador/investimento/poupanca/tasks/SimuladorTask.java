package br.com.sicredi.simulador.investimento.poupanca.tasks;

import br.com.sicredi.simulador.investimento.poupanca.helpers.Formater;
import br.com.sicredi.simulador.investimento.poupanca.objects.DadosSimulacaoDTO;
import br.com.sicredi.simulador.investimento.poupanca.pageObjects.ResultadoSimularPage;
import br.com.sicredi.simulador.investimento.poupanca.pageObjects.SimularPage;
import org.openqa.selenium.WebDriver;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class SimuladorTask {

    private WebDriver driver;
    private SimularPage simularPage;
    private ResultadoSimularPage resultadoSimularPage;

    public SimuladorTask(WebDriver _driver) {
        this.driver = _driver;
        simularPage = new SimularPage(driver);
        resultadoSimularPage = new ResultadoSimularPage(driver);
    }

    public void selecionarPerfil(Boolean paraVoce) {
        if(paraVoce.equals(true)){
            simularPage.paraVoceCheckBox().click();
        } else {
            simularPage.paraEmpresaCheckBox().click();
        }
    }

    public void preencherFormulario(DadosSimulacaoDTO dadosAplicacao){
        assertThat(simularPage.simuladorFormExist(), is(true));

        simularPage.valorAplicarInput().sendKeys(Formater.bigDecimalToString(dadosAplicacao.getValorAplicacaoInicial()));
        simularPage.valorInvestirInput().sendKeys(Formater.bigDecimalToString(dadosAplicacao.getValorAPoupar()));
        simularPage.tempoInput().sendKeys(dadosAplicacao.getPeriodo().toString());
    }

    public void realizarSimulacao(){
        simularPage.simularButton().click();
    }

    public void refazerSimulacao(){
        resultadoSimularPage.refazerSimulacaoButton().click();
    }

    public void validarPreenchimentoObrigatorioFormulario(){
        assertThat(simularPage.valorInvestirErrorLabel().getText(),is("Valor mínimo de 20.00"));
        assertThat(simularPage.valorAplicarErrorLabel().getText(),is("Valor mínimo de 20.00"));
        assertThat(simularPage.tempoErrorLabel().getText(),is("Obrigatório"));
    }

    public void validarValorMinimoFormulario(){
        assertThat(simularPage.valorInvestirErrorLabel().getText(),is("Valor mínimo de 20.00"));
        assertThat(simularPage.valorAplicarErrorLabel().getText(),is("Valor mínimo de 20.00"));
    }

    public void validarSimulacao(DadosSimulacaoDTO dadosAplicacao){
//        //TODO Conforme a regra, o valor deveria estar correto
//        BigDecimal taxaAoMes = BigDecimal.valueOf(0.005);
//        BigDecimal rendimento = BigDecimal.ZERO;
//        if(dadosAplicacao.getMeses().equals(true)){
//            rendimento = BigDecimal.valueOf(dadosAplicacao.getValorAplicacaoInicial());
//            for (int index = 1; index <= dadosAplicacao.getPeriodo(); index++){
//                rendimento = rendimento.add(BigDecimal.valueOf(dadosAplicacao.getValorAPoupar()).add(
//                        (rendimento.multiply(taxaAoMes)))).setScale(2, RoundingMode.HALF_EVEN);
//            }
//        }

        assertThat(resultadoSimularPage.simulacaoForm(), is(true));
        assertThat(resultadoSimularPage.valorSimulacaoLabel().getText(), is(notNullValue()));
        assertThat(resultadoSimularPage.descricaoMesesLabel().getText(), is(dadosAplicacao.getPeriodo().toString().concat(" meses")));
    }

    public void validarRefazerSimulacao(){
        assertThat(simularPage.simuladorFormExist(), is(true));
    }
}

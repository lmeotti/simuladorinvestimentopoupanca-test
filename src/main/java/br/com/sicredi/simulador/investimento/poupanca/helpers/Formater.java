package br.com.sicredi.simulador.investimento.poupanca.helpers;

import java.text.DecimalFormat;

public class Formater {

    public static String bigDecimalToString(Double value){
        DecimalFormat df = new DecimalFormat("###,###.00");
        return df.format(value);
    }
}

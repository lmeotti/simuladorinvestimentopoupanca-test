package br.com.sicredi.simulador.investimento.poupanca.pageObjects;

import br.com.sicredi.simulador.investimento.poupanca.helpers.WaitElements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ResultadoSimularPage {

    private WebDriver driver;

    public ResultadoSimularPage(WebDriver _driver) {
        this.driver = _driver;
    }

    public WebElement descricaoMesesLabel(){
        return WaitElements.isVisible(driver, By.cssSelector("div.blocoResultadoSimulacao > span.texto > strong"), 5);
    }

    public WebElement valorSimulacaoLabel(){
        return WaitElements.isVisible(driver, By.cssSelector("div.blocoResultadoSimulacao > span.valor"), 5);
    }

    public Boolean simulacaoForm(){
        return WaitElements.exist(driver, By.cssSelector("div.formularioBloco.formularioBlocoResultado"), 5);
    }

    public WebElement refazerSimulacaoButton(){
        return WaitElements.isVisible(driver, By.cssSelector("div.blocoResultadoSimulacao > a.btn.btnAmarelo.btnRefazer"), 5);
    }
}

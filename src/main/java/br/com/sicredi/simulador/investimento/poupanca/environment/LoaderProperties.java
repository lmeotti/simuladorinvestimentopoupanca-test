package br.com.sicredi.simulador.investimento.poupanca.environment;

import org.apache.commons.configuration.PropertiesConfiguration;

public class LoaderProperties {
    private static PropertiesConfiguration config = loadProperties();

    private static PropertiesConfiguration loadProperties() {

        PropertiesConfiguration propertiesConfiguration = new PropertiesConfiguration();

            try {
                propertiesConfiguration.load("src/main/resources/application.properties");
            } catch (org.apache.commons.configuration.ConfigurationException e) {
                e.printStackTrace();
            }

        return propertiesConfiguration;
    }

    public static String getUrlSimulador(){
        return config.getString("url.simulador");
    }

    public static String getBaseUri(){
        return config.getString("simulador.base.uri.service");
    }

    public static String getBasePath(){
        return config.getString("simulador.base.path.service");
    }
}

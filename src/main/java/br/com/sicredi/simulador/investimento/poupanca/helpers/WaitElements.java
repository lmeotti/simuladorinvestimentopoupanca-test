package br.com.sicredi.simulador.investimento.poupanca.helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitElements {

    public static WebElement isVisible(WebDriver driver, By locator, long waitTimeSeconds){
        WebDriverWait wait = new WebDriverWait(driver, waitTimeSeconds);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static WebElement isClickable(WebDriver driver, By locator, long waitTimeSeconds){
        WebDriverWait wait = new WebDriverWait(driver, waitTimeSeconds);
        return wait.until(ExpectedConditions.elementToBeClickable(locator));
    }

    public static Boolean exist(WebDriver driver, By locator, long waitTimeSeconds){
        WebDriverWait wait = new WebDriverWait(driver, waitTimeSeconds);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(locator)).isEnabled();
    }
}

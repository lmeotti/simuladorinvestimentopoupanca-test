package br.com.sicredi.simulador.investimento.poupanca.specifications;

import br.com.sicredi.simulador.investimento.poupanca.environment.LoaderProperties;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

public class RequestSpec {

    public static RequestSpecification specGetSimulacao(){
        return new RequestSpecBuilder()
                        .setRelaxedHTTPSValidation()
                        .setBaseUri(LoaderProperties.getBaseUri())
                        .setBasePath(LoaderProperties.getBasePath())
                        .setContentType("application/json")
                    .build()
        ;
    }
}

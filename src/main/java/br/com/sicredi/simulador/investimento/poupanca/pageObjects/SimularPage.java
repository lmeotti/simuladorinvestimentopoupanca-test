package br.com.sicredi.simulador.investimento.poupanca.pageObjects;

import br.com.sicredi.simulador.investimento.poupanca.helpers.WaitElements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SimularPage {

    private WebDriver driver;

    public SimularPage(WebDriver _driver){
        this.driver = _driver;
    }

    public Boolean simuladorFormExist(){
        return WaitElements.exist(driver, By.className("formularioBloco"), 5);
    }

    public WebElement valorAplicarInput(){
        return WaitElements.isClickable(driver, By.id("valorAplicar"), 5);
    }

    public WebElement valorInvestirInput(){
        return WaitElements.isClickable(driver, By.id("valorInvestir"), 5);
    }

    public WebElement tempoInput(){
        return WaitElements.isClickable(driver, By.id("tempo"), 5);
    }

    public WebElement simularButton(){
        return WaitElements.isClickable(driver, By.xpath("//button[@type='submit']"), 5);
    }

    public WebElement limparFormularioButton(){
        return WaitElements.isClickable(driver, By.xpath("//*[@id='formInvestimento']//a[@class='btn btnLimpar']"), 5);
    }

    public WebElement paraVoceCheckBox(){
        return WaitElements.isClickable(driver, By.xpath("//*[@id='formInvestimento']//input[@value='paraVoce']"), 5);
    }

    public WebElement paraEmpresaCheckBox(){
        return WaitElements.isClickable(driver, By.xpath("//*[@id='formInvestimento']//input[@value='paraEmpresa']"), 5);
    }

    public WebElement valorAplicarErrorLabel(){
        return WaitElements.isClickable(driver, By.id("valorAplicar-error"), 5);
    }

    public WebElement valorInvestirErrorLabel(){
        return WaitElements.isClickable(driver, By.id("valorInvestir-error"), 5);
    }

    public WebElement tempoErrorLabel(){
        return WaitElements.isClickable(driver, By.id("tempo-error"), 5);
    }
}

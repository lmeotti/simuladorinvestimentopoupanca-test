package br.com.sicredi.simulador.investimento.poupanca.objects;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class DadosSimulacaoDTO {

    private Double valorAplicacaoInicial;
    private Double valorAPoupar;
    private Boolean meses;
    private Integer periodo;

}
